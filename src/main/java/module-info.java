module bitcon {
    requires transitive javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
    requires java.base;

    opens cr.ac.utn.bitcon to javafx.fxml;
    exports cr.ac.utn.bitcon;
    exports cr.ac.utn.bitcon.logic;
}
