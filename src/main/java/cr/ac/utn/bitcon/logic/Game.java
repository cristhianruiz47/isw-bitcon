/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 * @author crist
 */
public class Game {
    private GameType type;
    private List<Integer> ballList;
    private List<Board> boardList;
    private List<Integer> drawList;
    private Random r = new Random();

    /**
     *
     * @param type
     */
    public Game(GameType type) {
        this.type = type;
        drawList = new ArrayList<>();

        initBallList();
        initBoardList();
    }

    private void initBallList() {
        ballList = new ArrayList<>();

        for (int i = 1; i <= 95; i++) {
            ballList.add(i);
        }
    }

    private void initBoardList() {
        boardList = new ArrayList<>();

        for (int i = 1000; i < 1100; i++) {
            boardList.add(new Board(""+i));
        }
    }

    private boolean hasWinners() {
        return getWinners().size() > 0;
    }

    private List<Board> getWinners() {
        return boardList.stream().filter(b -> b.isWinner(type, drawList)).collect(Collectors.toList());
    }

    /**
     *
     * @return the board list
     */
    public List<Board> getBoardList() {
        return Collections.unmodifiableList(boardList);
    }

    /**
     *
     * @return the draw list
     */
    public List<Integer> getDrawList() {
        return Collections.unmodifiableList(drawList);
    }

    /**
     *
     * @return the ball list
     */
    public List<Integer> getBallList() {
        return Collections.unmodifiableList(ballList);
    }

    /**
     * Draws a randomly picked number from the pool
     * @return The randomly picked number
     * @throws GameEndedException When at least one winner exists, which means the game has ended
     */
    public int draw() throws GameEndedException {
        if (ballList.size() < 1) {
            // No more balls to draw. This should never happen. Bug ?
            throw new UnsupportedOperationException("No more balls to draw");
        } else {
            var ball = ballList.remove(r.nextInt(ballList.size()));
            drawList.add(ball);

            if (hasWinners()) {
                throw new GameEndedException(
                    String.format("The game has eneded. Number of winners %d", getWinners().size()), getWinners());
            }

            return ball;
        }
    }

    /**
     * Draw a specific number from the pool
     * @param number The number to draw
     * @throws GameEndedException When at least one winner exists, which means the game has ended
     * @throws IllegalArgumentException When the requested number is not available in the pool
     */
    public void draw(int number) throws GameEndedException {
        if (ballList.size() < 1) {
            // No more balls to draw. This should never happen. Bug ?
            throw new UnsupportedOperationException("No hay mas fichas para extraer");
        } else if (!ballList.contains(number)) {
            // Requested number does not exists, it's probably drawn already
            if(number>0 && number<=95){
                throw new IllegalArgumentException(String.format("Número %d ya no está disponible, ya fue extraído", number));
            }
            else{
                throw new IllegalArgumentException(String.format("Debes ingresar un número valido, entre 1-100", number));
            }
        } else {
            // we need to obtain first the index id to be possible remove, cause remove works under index
            var drewBallID = ballList.indexOf(number);
            var ball = ballList.remove(drewBallID);
            drawList.add(ball);

            if (hasWinners()) {
                throw new GameEndedException(
                        String.format("El juego ha terminado. Numero de ganadores %d", getWinners().size()), getWinners());
            }
        }
    }

    /**
     *
     * @return the current game type
     */
    public GameType getType() {
        return type;
    }
}
