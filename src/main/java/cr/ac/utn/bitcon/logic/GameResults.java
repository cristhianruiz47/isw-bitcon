/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon.logic;

import java.util.List;

/**
 *
 * @author crist
 */
public class GameResults {
    private static boolean playerWins;
    private static boolean computerWins;
    private static List<Board> winnerBoards;
    private static List<Board> allBoardList;
    private static List<Integer> drawList;
  
    /**
     *
     * @param pList
     */
    public static void setDrawList(List<Integer> pList){
        drawList = pList;
    }

    /**
     *
     * @return the draw list
     */
    public static List<Integer> getDrawList(){
        return drawList;
    }    

    /**
     *
     * @param pBoard, set the winner board list
     */
    public static void setWinnerBoards(List<Board> pBoard){
        winnerBoards = pBoard;
    }

    /**
     *
     * @param pBoard receive all board list
     */
    public static void setAllBoardList(List<Board> pBoard){
        allBoardList = pBoard;
    }

    /**
     *
     * @return all board list
     */
    public static List<Board> getAllBoardList(){
        return allBoardList;
    }

    /**
     *
     * @return the list of the winners boards
     */
    public static List<Board> getWinnerBoards(){
        return winnerBoards;
    }

    /**
     *
     * @param p_win, to define if winner is a player
     */
    public static void setPlayerWins(boolean p_win){
        playerWins=true;
    }

    /**
     *
     * @param p_win, ti define if computer is the winner
     */
    public static void setComputerWins(boolean p_win){
        computerWins=true;
    }   
    
    /**
     *
     * @return, return a true or false to check if player is a winner
     */
    public static boolean getPlayerWins(){
        return playerWins;
    }

    /**
     *
     * @return, return a true or false if computer is a winner
     */
    public static boolean getComputerWins(){
        return computerWins;
    }
}
