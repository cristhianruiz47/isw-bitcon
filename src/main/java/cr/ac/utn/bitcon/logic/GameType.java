/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon.logic;

/**
 *
 * @author crist
 */
public enum GameType {

    /**
     * this game type define a winner with the first line in horizontal
     */
    HORIZONTAL("Horizontal"),

    /**
     * this game type define a winner with the first line in vertical
     */
    VERTICAL("Vertical"),

    /**
     * this game type define a winner by complete the board
     */
    FULL("Completo"),

    /**
     *this game type define a winner with the first line diagonal
     */
    DIAGONALIZQ("Diagonal IZQ-DERE"),

    /**
     *this game type define a winner with the first line diagonal
     */
    DIAGONALDERE("Diagonal DERE-IZQ"),

    /**
     * this game type will define a winner by complete an X figure on the board
     */
    ONX("En X");

    private final String displayName;

    GameType(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
