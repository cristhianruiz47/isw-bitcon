/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon.logic;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crist
 */
public class Board {

    private int[][] matrix;
    private Integer radix;
    private String name;

    /**
     *
     * @param name, this variable will define the name of the board
     */
    public Board(String name) {
        this(name, 10);
    }

    /**
     * this method will return the base of the board
     * @param name, name of the board wanted
     * @param radix, radix of the base wanted
     */
    public Board(String name, int radix) {
        this.name = name;
        this.radix = radix;
        this.matrix = new int[6][6];
        for (int x = 0; x < this.matrix.length; x++) {
            for (int y = 0; y < this.matrix.length; y++) {
                switch(y){
                    case 0:
                        this.matrix[x][y] = getRandomNumber(1, 15);
                    break;
                    case 1:
                        this.matrix[x][y] = getRandomNumber(16, 31);
                    break;
                    case 2:
                        this.matrix[x][y] = getRandomNumber(32, 47);
                    break;
                    case 3:
                        this.matrix[x][y] = getRandomNumber(48, 63);
                    break;
                    case 4:
                        this.matrix[x][y] = getRandomNumber(64, 79);
                    break;                    
                    case 5:
                        this.matrix[x][y] = getRandomNumber(80, 95);
                    break;
                    default:
                }
            }
        }        
    }

    /**
     *
     * @return a copy of the matrix
     */
    public int[][] getMatrix() {
        return matrix.clone();
    }

    /**
     *
     * @return the name of the board
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * this method will assign a name to the board
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return the radix of the board
     */
    public int getRadix() {
        return radix;
    }

    /**
     *
     * @param type
     * @param drawList
     * @return
     */
    public boolean isWinner(GameType type, List<Integer> drawList) {
        switch (type) {
        case FULL:
            return this.isWinnerFull(drawList);
        case HORIZONTAL:
            return this.isWinnerHorizontal(drawList);
        case VERTICAL:
            return this.isWinnerVertical(drawList);
        case DIAGONALDERE:
            return this.isWinnerDiagonalDerecha(drawList);
        case DIAGONALIZQ:
            return this.isWinnerDiagonalIzquierda(drawList);   
        case ONX:
            return this.isWinnerX(drawList);                           
        default:
            throw new IllegalArgumentException("Unsupported game type");
        }
    }

    private boolean isWinnerFull(List<Integer> drawList) {
        var missed = new ArrayList<Integer>();

        for (int[] row : matrix) {
            for (int column : row) {
                if (!drawList.contains(column)) {
                    missed.add(column);
                }
            }
        }

        return missed.size() < 1;
    }

    private boolean isWinnerHorizontal(List<Integer> drawList) {
        // throw new UnsupportedOperationException();
        boolean horizontalAppears=false;
        int horizontalCounter=1;
        for (int[] row : matrix) {
            for (int column : row) {
                if(drawList.contains(column)){
                    horizontalCounter++;
                }
                else{
                    horizontalCounter--;
                }
            }
            if(horizontalCounter!=row.length+1){
                horizontalCounter=1;
            }
            else{
                horizontalAppears=true;
            }
        }        
        return horizontalAppears;
    }

    private boolean isWinnerVertical(List<Integer> drawList) {
        // throw new UnsupportedOperationException();
        boolean verticalAppears=false;
        int verticalCounter=1;
        int column;
        for(column=0;column<matrix[0].length;column++){
            for(int i = 0 ; i < matrix.length; i++){
                if(drawList.contains(matrix[i][column])){
                    verticalCounter++;
                }
                else{
                    verticalCounter--;
                }
            }
            if(verticalCounter!=matrix.length+1){
                verticalCounter=1;
            }
            else{
                verticalAppears=true;
            }            
        }
        return verticalAppears;
    }

    private boolean isWinnerX(List<Integer> drawList){
        boolean xAppears=false;
        int XValues=0;
        int n = matrix[0].length;
        int XRequirement=n*2;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j){
                    if(drawList.contains(matrix[i][j])){
                        XValues++;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i + j) == (n - 1)){
                    if(drawList.contains(matrix[i][j])){
                        XValues++;
                    }
                }
            }
        }

        if(XValues==XRequirement){
            xAppears=true;
        }
        return xAppears;
    }

    private boolean isWinnerDiagonalIzquierda(List<Integer> drawList){
        boolean diagonalAppears=false;
        int n = matrix[0].length;
        int DValues=0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j){
                    if(drawList.contains(matrix[i][j])){
                        DValues++;
                    }
                }
            }
        }
        if(DValues==n){
            diagonalAppears=true;
        }
        return diagonalAppears;
    }

    private boolean isWinnerDiagonalDerecha(List<Integer> drawList){
        boolean diagonalAppears=false;
        int n = matrix[0].length;
        int DValues=0;        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i + j) == (n - 1)){
                    if(drawList.contains(matrix[i][j])){
                        DValues++;
                    }
                }
            }
        }  
        if(DValues==n){
            diagonalAppears=true;
        }             
        return diagonalAppears;
    }    

    /**
     *
     * @param min
     * @param max
     * @return
     */
    public int getRandomNumber(int min, int max) {
        int theRandomNumber;
        do{
            theRandomNumber = (int) ((Math.random() * (max - min)) + min);
        }while(checkForValue(theRandomNumber));

        return theRandomNumber;
    }

    private boolean checkForValue(int val){
        boolean response = false;
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                if(this.matrix[i][j] == val){
                    response=true;
                }
            }
        }
        return response;
    }

    @Override
    public String toString() {
        return getName();
    }    
}
