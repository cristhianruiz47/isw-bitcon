/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon.logic;

import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDateTime;

/**
 *
 * @author crist
 */
public class GameSettings {
    private static final StringProperty name = new SimpleStringProperty();
    private static final ObjectProperty<GameType> gameType = new SimpleObjectProperty<>();
    private static final ObjectProperty<DrawType> drawType = new SimpleObjectProperty<>();
    private static final ObjectProperty<NumericSystem> numericSystem = new SimpleObjectProperty<>();
    private static final IntegerProperty boardAmount = new SimpleIntegerProperty();
    private static List<Board> playerboardList;
    private static List<Board> computerboardList;
    private static int baseNumericSystem;
    private static LocalDateTime startTime;
    private static LocalDateTime endTime;

    /**
     *
     * @param p_startTime, this variable save the start game hour
     */
    public static void setStartTime(LocalDateTime p_startTime){
        startTime = p_startTime;
    }

    /**
     *
     * @return, this method will return the start game time
     */
    public static LocalDateTime getStartTime(){
        return startTime;
    }

    /**
     *
     * @param p_endTime, this method will save the end game time
     */
    public static void setEndTime(LocalDateTime p_endTime){
        endTime = p_endTime;
    }

    /**
     *
     * @return this method will return the end game time
     */
    public static LocalDateTime getEndTime(){
        return endTime;
    }    

    /**
     *
     * @param pPlayerBoard, this method will save the board list owner for the player
     */
    public static void setPlayerboardList(List<Board> pPlayerBoard){
        playerboardList = pPlayerBoard;
    }

    /**
     *
     * @param pComputerBoard, this method will save the board list owner for the computer
     */
    public static void setComputerboardList(List<Board> pComputerBoard){
        computerboardList = pComputerBoard;
    }

    /**
     *
     * @return, to return the player board list
     */
    public static List<Board> getPlayerboardList(){
        return playerboardList;
    }

    /**
     *
     * @return the computer board list
     */
    public static List<Board> getComputerboardList(){
        return computerboardList;
    }

    /**
     *
     * @return the name of the game
     */
    public static StringProperty nameProperty() {
        return name;
    }

    /**
     *
     * @return, the current game type
     */
    public static ObjectProperty<GameType> gameTypeProperty() {
        return gameType;
    }

    /**
     *
     * @return the method of the balls
     */
    public static ObjectProperty<DrawType> drawTypeProperty() {
        return drawType;
    }

    /**
     *
     * @return the method of the balls
     */
    public static DrawType getDrawType() {
        return drawType.get();
    }

    /**
     *
     * @param value, define a draw type
     */
    public static void setDrawType(DrawType value) {
        drawType.set(value);
    }

    /**
     *
     * @return the current numeric system used
     */
    public static ObjectProperty<NumericSystem> numericSystemProperty() {
        return numericSystem;
    }

    /**
     *
     * @return, the base of the radix for the current game 
     */
    public static int getBaseNumericSystem() {
        baseNumericSystem = 10;
        switch(numericSystem.getValue()){
            case QUINARIO:
                baseNumericSystem=5;
            break;
            case OCTAL:
                baseNumericSystem=8;
            break;
            case DECIMAL:
                baseNumericSystem=10;
            break;
            case DUODEMCIMAL:
                baseNumericSystem=12;
            break;
            case HEXADECIMAL:
                baseNumericSystem=16;
            break;
            default:
                baseNumericSystem=10;
        }
        return baseNumericSystem;
    }    

    /**
     *
     * @return, how many boards the player choose
     */
    public static IntegerProperty boardAmountProperty() {
        return boardAmount;
    }

    /**
     *
     * @param fromNum
     * @param toNum
     * @param numtoConverted
     * @return
     */
    public static String getNumConverted(NumericSystem fromNum, NumericSystem toNum, String numtoConverted){
        String toReturn="";
        int decimal=0;
        int baseNumericSystem;
        switch(fromNum){
            case QUINARIO:
                baseNumericSystem=5;
            break;
            case OCTAL:
                baseNumericSystem=8;
            break;
            case DECIMAL:
                baseNumericSystem=10;
            break;
            case DUODEMCIMAL:
                baseNumericSystem=12;
            break;
            case HEXADECIMAL:
                baseNumericSystem=16;
            break;
            default:
                baseNumericSystem=10;            
        }
        //convert to decimal
        try{
            decimal = Integer.parseInt(numtoConverted,baseNumericSystem);
        }catch(NumberFormatException e){
            toReturn="ERROR";
        }

        String resultado="";
        switch(toNum){
            case QUINARIO:
                resultado = Integer.toString(decimal,5);
                toReturn=resultado;
            break;
            case OCTAL:
                resultado = Integer.toString(decimal,8);
                toReturn=resultado;
            break;
            case DECIMAL:
                resultado = Integer.toString(decimal,10);
                toReturn=resultado;
            break;
            case DUODEMCIMAL:
                resultado = Integer.toString(decimal,12);
                toReturn=resultado;
            break;
            case HEXADECIMAL:
                resultado = Integer.toString(decimal,16);
                toReturn=resultado;
            break;
            default:
                resultado = Integer.toString(decimal,10);
                toReturn=resultado;
        }        
        return toReturn;
    }
}
