/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon.logic;

import java.util.List;

/**
 *
 * @author crist
 */
public class GameEndedException extends Exception {
    private List<Board> winners;

    GameEndedException(String message, List<Board> winners) {
        super(message);
        this.winners = winners;
    }

    /**
     *
     * @return the winner board list
     */
    public List<Board> getWinners() {
        return winners;
    }
}
