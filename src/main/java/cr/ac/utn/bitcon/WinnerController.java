/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cr.ac.utn.bitcon;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import cr.ac.utn.bitcon.logic.Board;
import cr.ac.utn.bitcon.logic.GameResults;
import cr.ac.utn.bitcon.logic.GameSettings;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.geometry.HPos;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Winner controller will handle the view to print
 * the winners of the match
 *
 */
public class WinnerController implements Initializable {
    @FXML
    private Region anchorPaneWinner;
    @FXML
    private Button winnerTitle;
    @FXML
    private Button home;
    @FXML
    private ComboBox<Object> playerBoardsWinners;
    @FXML
    private ComboBox<Object> computerBoardsWinners;
    @FXML
    private GridPane gridPaneUsers;

    //Labels on Resumen
    @FXML
    private Label playerBoardCounter;
    @FXML
    private Label computerBoardCounter;
    @FXML
    private Label playerWinnerBoardCounter;
    @FXML
    private Label computerWinnerBoardCounter;
    @FXML
    private Label idPlayerWinnerBoardCounter;
    @FXML
    private Label idComputerWinnerBoardCounter;
    @FXML
    private Label mathTimerPlayer;
    @FXML
    private Label mathTimerComputer;
        
    // Obtain all boards created before and during the game
    List<Board> playerBoards = GameSettings.getPlayerboardList();
    List<Board> computerBoards = GameSettings.getComputerboardList();
    List<Board> winnerBoards = GameResults.getWinnerBoards();
    List<Board> winnerPlayerBoards=new ArrayList<>();
    List<Board> winnerComputerBoards=new ArrayList<>();

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        

        // Edit title
        if(GameResults.getPlayerWins()){
            winnerTitle.setText("Felicidades Jugador");
        }
        else{
            winnerTitle.setText("Felicidades Computer");
        }

        //for to fill a list with the player win boards
        for (Board i: winnerBoards) {
            if (playerBoards.contains(i)) {
                winnerPlayerBoards.add(new Board(""+i));
            }
        }

        
        //for to fill a list with the computer win boards
        for (Board i: winnerBoards) {
            if (computerBoards.contains(i)) {
                winnerComputerBoards.add(new Board(""+i));
            }
        }

        //System.out.println("ComputerBoards: " + winnerComputerBoards);
        //Fill comboBoxes
        playerBoardsWinners.setItems(FXCollections.observableArrayList(winnerPlayerBoards));
        computerBoardsWinners.setItems(FXCollections.observableArrayList(winnerComputerBoards));

        //Get how many winner boards has each user and print on dropdowns
        playerBoardsWinners.setPromptText(winnerPlayerBoards.size() + " Tableros ganador(es) para Player");
        computerBoardsWinners.setPromptText(winnerComputerBoards.size() + " Tableros ganador(es) para Computer");

        //Color default matrix
        for (int x = 0; x < 6; x++){
            for (int y = 0; y < 6; y++){
                Rectangle ap = new  Rectangle(35,35,35,35);
                ap.setFill(Color.GRAY);
                gridPaneUsers.add(ap, y, x);
                GridPane.setHalignment(ap, HPos.CENTER);
            }
        }   
        
        //Fill "Resumen"
        Duration gameDuration = Duration.between(GameSettings.getStartTime(), GameSettings.getEndTime());
        float gameSeconds = gameDuration.getSeconds();
        DecimalFormat df = new DecimalFormat("#.##");
        float gameTime;
        String timeUnit=" Minutos.";
        if(gameSeconds>60){
            gameTime = gameSeconds/60;
        }
        else{
            gameTime = gameSeconds;
            timeUnit = " Segundos.";
        }

        playerBoardCounter.setText(""+playerBoards.size());
        computerBoardCounter.setText(""+computerBoards.size());
        playerWinnerBoardCounter.setText(""+winnerPlayerBoards.size());
        computerWinnerBoardCounter.setText(""+winnerComputerBoards.size());
        idPlayerWinnerBoardCounter.setText(""+winnerPlayerBoards);
        idComputerWinnerBoardCounter.setText(""+winnerComputerBoards);
        mathTimerPlayer.setText(""+df.format(gameTime) + timeUnit);
        mathTimerComputer.setText(""+df.format(gameTime) + timeUnit);

    }     
    
    @FXML
    private void homeActionPerformed() throws IOException {
        App.setRoot("welcome");
    }  
    @FXML
    private void salirActionPerformed() throws IOException {
        System.exit(0);
    }      
    @FXML
    private void playerBoardsWinnersActionPerformed(){
        gridPaneUsers.getChildren().clear();        
        Object selectedBoard = playerBoardsWinners.getValue();
        int printBoard = winnerPlayerBoards.indexOf(selectedBoard);
        printMatrixActionPerformed(printBoard, "player");      
    }
    @FXML
    private void computerBoardsWinnersActionPerformed(){
        gridPaneUsers.getChildren().clear();        
        Object selectedBoard = computerBoardsWinners.getValue();
        int printBoard = winnerComputerBoards.indexOf(selectedBoard);
        printMatrixActionPerformed(printBoard, "computer");      
    }    

    private void printMatrixActionPerformed(int pBoard, String p_user){
        int theMatrix[][];
        theMatrix = winnerBoards.get(pBoard).getMatrix();
        // System.out.println("DRAW List: " + GameResults.getDrawList());
        for (int x = 0; x < theMatrix.length; x++){
            for (int y = 0; y < theMatrix[x].length; y++){
                Label label = new Label();
                Rectangle ap = new  Rectangle(35,35,35,35);
                label.setFont(Font.font("Consolas", FontWeight.BOLD, 16));
                label.setText(""+   Integer.toString(theMatrix[x][y],GameSettings.getBaseNumericSystem()));
                if(GameResults.getDrawList().contains(theMatrix[x][y])){
                    ap.setFill(Color.LIGHTGOLDENRODYELLOW);
                }
                else{
                    ap.setFill(Color.GRAY);
                }
                gridPaneUsers.add(ap, y, x);
                gridPaneUsers.add(label, y, x);
                GridPane.setHalignment(label, HPos.CENTER);
                GridPane.setHalignment(ap, HPos.CENTER);
            }
        } 
    }    
}