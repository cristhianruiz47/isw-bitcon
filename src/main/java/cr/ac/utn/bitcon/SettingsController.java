/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon;

import cr.ac.utn.bitcon.logic.GameSettings;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ValueRange;
import java.util.ResourceBundle;
import cr.ac.utn.bitcon.logic.DrawType;
import cr.ac.utn.bitcon.logic.GameType;
import cr.ac.utn.bitcon.logic.NumericSystem;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.Region;
import javafx.util.Duration;

/**
 * Settings controller define the settings
 * for the game, this controller represet the view
 * to configure the game
 *
 */
public class SettingsController implements Initializable {

    /**
     * define the region variable to set images
     * and get the FXML components to be used on the controller
     */
    public Region rootPane;
    @FXML
    private Button startButton;
    @FXML
    private ComboBox<DrawType> drawTypeComboBox;
    @FXML
    private ComboBox<NumericSystem> numericSystemComboBox;
    @FXML
    private TextField boardAmountTextField;
    @FXML
    private ComboBox<GameType> gameTypeComboBox;
    @FXML
    private TextField gameNameTextField;

    private final BooleanProperty settingsInvalid = new SimpleBooleanProperty(true);

    public void initialize(URL location, ResourceBundle resources) {
        var xPosition = new SimpleDoubleProperty(0);
        xPosition.addListener((observable, oldValue, newValue) -> setBackgroundPositions(rootPane, xPosition.get()));
        var timeline = new Timeline(new KeyFrame(Duration.ZERO, new KeyValue(xPosition, 0)),
                new KeyFrame(Duration.seconds(200), new KeyValue(xPosition, -15000)));
        timeline.play();

        settingsInvalid.bind(
            new When(
                GameSettings.drawTypeProperty().isNotNull()
                .and(GameSettings.numericSystemProperty().isNotNull())
                .and(GameSettings.gameTypeProperty().isNotNull())
                .and(GameSettings.nameProperty().isNotEmpty())
                .and(GameSettings.boardAmountProperty().greaterThan(0))
                .and(GameSettings.boardAmountProperty().lessThan(101))
            )
            .then(false)
            .otherwise(true)
        );

        GameSettings.numericSystemProperty().bind(numericSystemComboBox.valueProperty());
        GameSettings.gameTypeProperty().bind(gameTypeComboBox.valueProperty());
        GameSettings.drawTypeProperty().bind(drawTypeComboBox.valueProperty());
        GameSettings.nameProperty().bind(gameNameTextField.textProperty());

        startButton.disableProperty().bind(settingsInvalid);
        numericSystemComboBox.setItems(FXCollections.observableArrayList(NumericSystem.values()));
        gameTypeComboBox.setItems(FXCollections.observableArrayList(GameType.values()));
        drawTypeComboBox.setItems(FXCollections.observableArrayList(DrawType.values()));
        boardAmountTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if ("".equals(newValue)) {
                return;
            }

            try {
                var range = ValueRange.of(1, 100);
                var value = Integer.parseInt(newValue);

                if (range.isValidValue(value)) {
                    GameSettings.boardAmountProperty().set(value);
                } else {
                    throw new IllegalArgumentException("Invalid value. Must be between 1 and 100");
                }
            } catch (IllegalArgumentException e) {
                new Alert(AlertType.ERROR, e.getMessage()).showAndWait();
                boardAmountTextField.setText("1");
            }
        });
        boardAmountTextField.setTextFormatter(new TextFormatter<>(c -> {
            // Prevents non-numeric input
            if (c.getText().matches("\\D+")) {
                return null;
            } else {
                return c;
            }
        }));
    }

    private void setBackgroundPositions(Region region, double xPosition) {
        String style = "-fx-background-position: " + "left " + xPosition / 7 + "px bottom," + "left " + xPosition / 6
                + "px bottom," + "left " + xPosition / 5 + "px bottom," + "left " + xPosition / 4 + "px bottom,"
                + "left " + xPosition / 3 + "px bottom," + "left " + xPosition / 2 + "px bottom," + "left "
                + xPosition / 1 + "px bottom," + "left 0px bottom;";
        region.setStyle(style);
    }

    @FXML
    private void backButtonActionPerformed() throws IOException {
        App.setRoot("welcome");
    }

    @FXML
    private void startButtonActionPerformed() throws IOException {
        GameSettings.numericSystemProperty().unbind();
        GameSettings.gameTypeProperty().unbind();
        GameSettings.drawTypeProperty().unbind();
        GameSettings.nameProperty().unbind();
        GameSettings.setStartTime(LocalDateTime.now());
        App.setRoot("game");
    }

    @FXML
    private void resetForm() {
        gameTypeComboBox.valueProperty().set(null);
        numericSystemComboBox.valueProperty().set(null);
        drawTypeComboBox.valueProperty().set(null);
        boardAmountTextField.setText("");
    }
}
