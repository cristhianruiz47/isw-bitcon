/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cr.ac.utn.bitcon;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

/**
 * Welcome controller is the first controller executed
 * this method will print the animation images and the "comenzar"
 * button
 *
 */
public class WelcomeController implements Initializable {
    @FXML
    private Region content;
    @FXML
    private Button comenzar;

    /**
     * Initializes the controller class.
     * @param location, is the location for the images
     * @param resources, is the source of the images
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DoubleProperty xPosition = new SimpleDoubleProperty(0);
        xPosition.addListener((observable, oldValue, newValue) -> setBackgroundPositions(content, xPosition.get()));
        Timeline timeline = new Timeline(new KeyFrame(Duration.ZERO, new KeyValue(xPosition, 0)),new KeyFrame(Duration.seconds(200), new KeyValue(xPosition, -15000)));
        timeline.play();
    }

    //Overlaping
    private void setBackgroundPositions(Region region, double xPosition) {
    String style = "-fx-background-position: " +
            "left " + xPosition/7 + "px bottom," +
            "left " + xPosition/6 + "px bottom," +
            "left " + xPosition/5 + "px bottom," +
            "left " + xPosition/4 + "px bottom," +
            "left " + xPosition/3 + "px bottom," +
            "left " + xPosition/2 + "px bottom," +
            "left " + xPosition + "px bottom;";
            region.setStyle(style);
    }
    @FXML
    private void comenzarActionPerformed() throws IOException {
        //Sound Free music on start Game
        Media hit = new Media(getClass().getResource("sounds/start.mp3").toExternalForm());
        MediaPlayer mediaPlayerStart = new MediaPlayer(hit);
        mediaPlayerStart.play();
        App.setRoot("settings");
    }
}

