/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cr.ac.utn.bitcon;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import cr.ac.utn.bitcon.logic.Board;
import cr.ac.utn.bitcon.logic.DrawType;
import cr.ac.utn.bitcon.logic.Game;
import cr.ac.utn.bitcon.logic.GameEndedException;
import cr.ac.utn.bitcon.logic.GameResults;
import cr.ac.utn.bitcon.logic.GameSettings;
import cr.ac.utn.bitcon.logic.NumericSystem;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
//import java.util.stream.Collectors;

/**
 * FXML Controller class
 * GameController represent the view in wich
 * the game is initialized, the windows when u can
 * draw balls and u can see the boards
 *
 */
public class GameController implements Initializable {
    // Here initialize all components on the game view
    @FXML
    private Button titlePlayGame;
    @FXML
    private TextField textFieldCurrentBall;
    @FXML
    private Button buttonRequestNumber;
    @FXML
    private ComboBox<Object> comboBoxAllBoards;
    @FXML
    private ComboBox<Object> comboBoxPlayerBoards;
    @FXML
    private TextArea TextAreaDrawList;
    @FXML
    private GridPane gridPaneComputer;
    @FXML
    private GridPane gridPanePlayer;

    //Conversor Items
    @FXML
    private TextField textFieldNumberToTraslation;
    @FXML
    private ComboBox<NumericSystem> comboBoxUnitItemsFrom; 
    @FXML
    private ComboBox<NumericSystem> comboBoxUnitItemsTo;
    @FXML
    private TextField textFieldNumberResult;
    @FXML
    private Button buttonConvertir;

    //Configuraciones Tab
    @FXML
    private Label labelConfigFichas;
    @FXML
    private Label labelConfigTipoDeJuego;
    @FXML
    private Label labelConfigSistemaNumerico;
    @FXML
    private Label labelConfigTablerosJugador;
    @FXML
    private Label labelConfigTablerosComputer;
    @FXML
    private Label gameTimeElapsed;


    private int boardAmount;
    private List<Board> computerBoardList;
    private List<Board> playerBoardList;
    Random rand = new Random();

    // the variable g is create, here is when the game is configured
    // the only value necessary to create the game is the game type
    // previusly defined on the settings controller
    Game g = new Game(GameSettings.gameTypeProperty().getValue());

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // set the title for the game, this is the "Nombre del juego"
        titlePlayGame.setText(GameSettings.nameProperty().getValue());

        // Set MANUAL or AUTOMATIC Game PIECES
        // GameSettings.drawTypeProperty().set(DrawType.AUTOMATIC);
        if(GameSettings.drawTypeProperty().get() == DrawType.AUTOMATIC){
            textFieldCurrentBall.setEditable(false);
            textFieldCurrentBall.setText(";)");
            buttonRequestNumber.setDisable(false);
            buttonRequestNumber.setText("Siguiente \n Número");
        }
        else{
            textFieldCurrentBall.setEditable(true);
            textFieldCurrentBall.setText("");
            buttonRequestNumber.setDisable(false);
            buttonRequestNumber.setText("Ingresar \n Número");
        }

        boardAmount = GameSettings.boardAmountProperty().getValue();
        playerBoardList = new ArrayList<Board>();
        computerBoardList = new ArrayList<Board>();

        //For to create a clone from BoardList
        for(int i=0;i<g.getBoardList().size();i++){
            computerBoardList.add(g.getBoardList().get(i));
        }
        
        //For to remove random items from computerBoardList and add to playerBoardList
        for(int i=0;i<boardAmount;i++){
            int randomIndex = rand.nextInt(computerBoardList.size());
            playerBoardList.add(computerBoardList.get(randomIndex));
            computerBoardList.remove(randomIndex);
        }

        //Define the Attrs on GameSettings
        GameSettings.setComputerboardList(computerBoardList);
        GameSettings.setPlayerboardList(playerBoardList);
        GameResults.setAllBoardList(g.getBoardList());

        //Define comboBox Values
        comboBoxAllBoards.setItems(FXCollections.observableArrayList(GameSettings.getComputerboardList()));
        comboBoxPlayerBoards.setItems(FXCollections.observableArrayList(GameSettings.getPlayerboardList()));   

        //Populate matrix on first view
        //Player Matrix
        Board firstPlayerBoard = playerBoardList.get(0);
        printPlayerBoard(firstPlayerBoard);
         
        // Computer matrix
        Board firstComputerBoard = computerBoardList.get(0);
        printComputerBoard(firstComputerBoard);

        //Conversor Tab
        comboBoxUnitItemsFrom.setItems(FXCollections.observableArrayList(NumericSystem.values()));
        comboBoxUnitItemsTo.setItems(FXCollections.observableArrayList(NumericSystem.values()));
        
    }

    //Set wich board will show the view (For player)
    private void printPlayerBoard(Board pPlayerBoard){
        int idBoardPlayer = g.getBoardList().indexOf(pPlayerBoard);
        comboBoxPlayerBoards.setValue(g.getBoardList().get(idBoardPlayer));
        int playerMatrix[][] = g.getBoardList().get(idBoardPlayer).getMatrix();
        for (int x = 0; x < playerMatrix.length; x++){
            for (int y = 0; y < playerMatrix[x].length; y++){
                Label label = new Label();
                Rectangle ap = new  Rectangle(35,35,35,35);
                label.setFont(Font.font("Consolas", FontWeight.BOLD, 16));
                label.setText(""+   Integer.toString(playerMatrix[x][y],GameSettings.getBaseNumericSystem()));
                if(checkIfDrawBall(playerMatrix[x][y])){
                    ap.setFill(Color.LIGHTGOLDENRODYELLOW);
                }
                else{
                    ap.setFill(Color.GRAY);
                }
                gridPanePlayer.add(ap, y, x);
                gridPanePlayer.add(label, y, x);
                GridPane.setHalignment(label, HPos.CENTER);
                GridPane.setHalignment(ap, HPos.CENTER);
            }
        } 
    }

    //Set wich board will show the view (For computer)
    private void printComputerBoard(Board pComputerBoard){
        int idBoardComputer = g.getBoardList().indexOf(pComputerBoard);
        comboBoxAllBoards.setValue(g.getBoardList().get(idBoardComputer));
        int computerMatrix[][] = g.getBoardList().get(idBoardComputer).getMatrix();
        for (int x = 0; x < computerMatrix.length; x++){
            for (int y = 0; y < computerMatrix[x].length; y++){
                Label label = new Label();
                Rectangle ap = new  Rectangle(35,35,35,35);
                label.setFont(Font.font("Consolas", FontWeight.BOLD, 16));
                label.setText(""+   Integer.toString(computerMatrix[x][y],GameSettings.getBaseNumericSystem()));
                if(checkIfDrawBall(computerMatrix[x][y])){
                    ap.setFill(Color.LIGHTGOLDENRODYELLOW);
                }
                else{
                    ap.setFill(Color.GRAY);
                }
                gridPaneComputer.add(ap, y, x);
                gridPaneComputer.add(label, y, x);
                GridPane.setHalignment(label, HPos.CENTER);
                GridPane.setHalignment(ap, HPos.CENTER);
            }
        } 
    }

    private boolean checkIfDrawBall(int p_numToCheck){
        boolean drew=false;
        if(g.getDrawList().contains(p_numToCheck)){
            drew=true;
        }
        return(drew);
    }

    //Request view a new board for player
    @FXML
    private void comboBoxPlayerBoardsActionPerformed() throws IOException {
        gridPanePlayer.getChildren().clear();        
        Object object = comboBoxPlayerBoards.getValue();
        Board playerBoard = playerBoardList.get(playerBoardList.indexOf(object));
        printPlayerBoard(playerBoard);
    }

    //Request view a new board for computer
    @FXML
    private void comboBoxComputerBoardsActionPerformed() throws IOException {
        gridPaneComputer.getChildren().clear();        
        Object object = comboBoxAllBoards.getValue();
        Board computerBoard = computerBoardList.get(computerBoardList.indexOf(object));
        printComputerBoard(computerBoard);
    }

    //Action Conversor
    @FXML
    private void buttonConvertirActionPerformed() throws IOException {
        if(textFieldNumberToTraslation.getText().isEmpty()|| comboBoxUnitItemsFrom.getSelectionModel().isEmpty() || comboBoxUnitItemsTo.getSelectionModel().isEmpty()){
            Alert alert = new Alert(AlertType.ERROR, "Debes ingresar un número primero y ambos sistemas númericos ", ButtonType.OK);
            alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
            alert.show();
        }
        else{
            NumericSystem getFromNumericSystem = comboBoxUnitItemsFrom.getSelectionModel().getSelectedItem();
            NumericSystem getToNumericSystem = comboBoxUnitItemsTo.getSelectionModel().getSelectedItem();
            String numberToConversion = textFieldNumberToTraslation.getText();
            String setValue = GameSettings.getNumConverted(getFromNumericSystem, getToNumericSystem, numberToConversion);
            textFieldNumberResult.setText(""+setValue);
        }
    }

    @FXML
    private void configuracionActionChange() throws IOException {
        //Configuraciones Tab
        labelConfigFichas.setText(""+GameSettings.getDrawType());
        labelConfigTipoDeJuego.setText(""+GameSettings.gameTypeProperty().get());
        labelConfigSistemaNumerico.setText(""+GameSettings.numericSystemProperty().get());
        labelConfigTablerosJugador.setText(""+GameSettings.getPlayerboardList().size());
        labelConfigTablerosComputer.setText(""+GameSettings.getComputerboardList().size());

        Duration gameDuration = Duration.between(GameSettings.getStartTime(), LocalDateTime.now());
        float gameSeconds = gameDuration.getSeconds();
        DecimalFormat df = new DecimalFormat("#.##");
        float gameTime;
        String timeUnit=" Minutos.";
        if(gameSeconds>60){
            gameTime = gameSeconds/60;
        }
        else{
            gameTime = gameSeconds;
            timeUnit = " Segundos.";
        }        
        gameTimeElapsed.setText(""+ df.format(gameTime) + " " + timeUnit);        
    }

    //On action throws buttonRequestNumber {
    @FXML
    private void buttonRequestNumberActionPerformed() throws IOException {
        if(GameSettings.drawTypeProperty().get() == DrawType.AUTOMATIC)
        {
            try {
                // while(true){
                    textFieldCurrentBall.setText(String.format(""+g.draw()));
                // }
            }catch (GameEndedException e) {
                //System.out.println(e.getMessage());
                //System.out.println(e.getWinners().stream().map(b -> b.getName()).collect(Collectors.joining(",")));
                //System.exit(0);
                // System.out.println(e.getMessage());
                // System.out.println(e.getWinners().stream().map(b -> b.getName()).collect(Collectors.joining(",")));

                //save draw list to access from gameresults (for painting matrix)
                GameResults.setDrawList(g.getDrawList());
                setEndGameTime();
                //save list of boards that won and set title for winners view
                findWinners(e.getWinners());
                
                App.setRoot("winner");
                
            }
        }
        else
        {
            try {
                if(!textFieldCurrentBall.getText().isEmpty()){
                    g.draw(Integer.parseInt(textFieldCurrentBall.getText()));
                    textFieldCurrentBall.setText("");
                }
                else{
                    Alert alert = new Alert(AlertType.ERROR, "Debes ingresar un número valido, entre 1-100", ButtonType.OK);
                    alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                    alert.show();
                }
            }catch (GameEndedException e) {
                GameResults.setDrawList(g.getDrawList());
                findWinners(e.getWinners());
                setEndGameTime();
                App.setRoot("winner");
                //System.out.println(e.getMessage());
                //System.out.println(e.getWinners().stream().map(b -> b.getName()).collect(Collectors.joining(",")));
            }
            catch (IllegalArgumentException i){
                // new Alert(AlertType.ERROR, "test").showAndWait();
                Alert alert = new Alert(AlertType.ERROR, i.getMessage(), ButtonType.OK);
                alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                alert.show();
            }
        }


        TextAreaDrawList.setText("Ya salierón: " + g.getDrawList());

        //reload grid player
        gridPanePlayer.getChildren().clear();        
        Object object = comboBoxPlayerBoards.getValue();
        Board playerBoard = playerBoardList.get(playerBoardList.indexOf(object));
        printPlayerBoard(playerBoard);

        //reload grid computer
        gridPaneComputer.getChildren().clear();        
        Object object1 = comboBoxAllBoards.getValue();
        Board computerBoard = computerBoardList.get(computerBoardList.indexOf(object1));
        printComputerBoard(computerBoard);        
    }

    private void findWinners(List<Board> p_WinnerList){
        List<Board> winnersBoard = p_WinnerList;
        GameResults.setWinnerBoards(winnersBoard);
        boolean checkPlayerBoard = (GameSettings.getPlayerboardList().containsAll(winnersBoard));

        //set title winners.fxml
        if(checkPlayerBoard){
            GameResults.setPlayerWins(true);
        }        
    }

    private void setEndGameTime(){
        GameSettings.setEndTime(LocalDateTime.now());
    }
}
