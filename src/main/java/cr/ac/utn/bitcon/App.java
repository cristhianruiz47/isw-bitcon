/*
 *     Copyright 2021 Cristhian Ruiz Hernandez @ cristhianruiz47@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cr.ac.utn.bitcon;

import java.io.IOException;
import javafx.scene.image.Image;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
// import javafx.scene.media.Media;
// import javafx.scene.media.MediaPlayer;

/**
 * Main JavaFX Method
 * this method will define the params to create the
 * window size, the first scene to load, and the icon and
 * title to display the frames
 * 
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        
        // This block of code can give u a music starting on the game
        // Sound Free music on start Game
        // Media welcomeSound = new Media(getClass().getResource("sounds/bip.mp3").toExternalForm());
        // MediaPlayer mediaPlayer = new MediaPlayer(welcomeSound);
        // mediaPlayer.play();

        // Start the first scene
        scene = new Scene(loadFXML("welcome"));
        stage.setScene(scene);

        // Set Minimun size for window
        stage.setMinHeight(720);
        stage.setMinWidth(1280);

        // Set icon and title for the window
        Image icon = new Image(getClass().getResourceAsStream("img/marco.png"));
        stage.getIcons().add(icon);
        stage.setTitle("BitCon - Cristhian Ruiz Hdez");        

        // Show stage
        stage.show();
    }

    // This method is used to be posible change the scene
    // u can call setRoot and send a FXML file to show
    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     *
     * @param args
     * The program start here
     */
    public static void main(String[] args) {
        launch();
    }
}
