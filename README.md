<div style="text-align:center"><img src="screenshots/titulo.png" /></div>

Universidad técnica Nacional | cristhianruiz47@gmail.com 
#
<div style="text-align:center"><img src="screenshots/manual.png" /></div>

#

## Resumen

Bitcon, es un juego de azar, que simula el clásico Bingo, en esta versión del juego seremos capaces de navegar entre varias maneras de juego, así mismo enfocado en un usuario informático tendremos la facilidad de utilizar diferentes sistemas numéricos, para dar mas emoción. 

Tendremos la posibilidad de jugar contra maquina escogiendo tableros asignados aleatoriamente para así hacer frente a nuestro computador, que tendrá las mismas posibilidades de ganar que nosotros.

A continuación, te dejaremos un guía para que puedas desenvolverte de una manera mas efectiva, especialmente entre nuestras múltiples configuraciones, que sin duda alguna te harán pasar un rato muy divertido.

## Introducción

A primera instancia, cuando iniciemos el juego nos mostrará una pequeña animación, la cual tendrá movimiento por unos segundos, posterior a esto las imágenes se detendrán. En esta parte solo tendremos disponible un botón, el cual indica: “Comenzar”, será este el que deberemos crackear para proceder al siguiente apartado.

<div style="text-align:center"><img src="screenshots/comenzar.png" /></div>

#
## Configuraciones
<div style="text-align:center"><img src="screenshots/configuraciones.png" /></div>
</br>
</br>

### MÉTODO DE FICHAS 
Esta opción nos permitirá escoger el método en el cual cada número (ficha), aparecerá en el juego, acá podremos escoger entre manual y automático, este último nos habilitara un botón el cual al ser presionado nos traerá un numero generado de manera aleatoria, a diferencia del modo manual, el cual lo que nos habilitara será un espacio espacial para poder ingresar el número que deseamos, este es recomendado por si tenemos alguna manera de genera los números, como lo sería la típica “tómbola”, de los juegos de lotería. 

### TIPO DE JUEGO 
Acá podremos incursionar entre varios tipos de juego, como lo mencionamos anteriormente, este juego se basa en el típico bingo, el cual consiste en completar una meta de números, esta meta podría ser desde: obtener el tablero completo (completo), obtener cualquier fila vertical (vertical), obtener cualquier fila horizontal (horizontal), Obtener una diagonal de izquierda a derecha (Diagonal izquierda), obtener la diagonal derecha (diagonal derecha) o por ultimo podremos jugar al tablero que obtenga de primero la figura X de extremo a extremo (en X).
A continuación, ejemplos de tableros ganadores para cada uno de los tipos de juego:
</br>
</br>

<div style="text-align:center"><img src="screenshots/tablas.png" /></div>
</br>
</br>

### NUMERO DE TABLEROS DESEADOS 

En el juego existen un total de 100 tableros con combinaciones de números diferentes, en la modalidad en la que esta desarrollada esta aplicación puedes escoger cuantos tableros deseas para enfrentarte con la máquina, mas no podrás escoger cuales tableros, solo la cantidad.

### SISTEMA NÚMERICO

En este apartado podremos definir la base de los números que aparecen en nuestra interfaz, podremos escoger observar en:

1.	Octal (Base 8)
2.	Decimal (Base 10)
3.	Hexadecimal (Base 16)
4.	Duodecimal (Base 12)
5.	Quinario (Base 5)


### NOMBRE DEL JUEGO

Acá podremos ingresar cualquier nombre personalizado, este representara como identificador la partida, no es necesario ni incluye en los resultados de los encuentros.

### JUGAR

Una vez tengamos definidas todas nuestras configuraciones, y las mismas hayan sido validada, el botón que indica “Jugar” se habilitara permitiéndonos proceder al encuentro.

</br>
</br>

# 
## JUEGO

<div style="text-align:center"><img src="screenshots/versus.png" /></div>
</br>
</br>

Una vez la partida inicie, podremos observar varias cosas en nuestro entorno, una de ellas, y a primera instancia, veremos en el titulo el nombre del juego que asignamos en el apartado de configuraciones.

En la parte inferior de la pantalla logramos observar una barra, esta contiene todas las fichas que “ya salieron”, así podremos validar cuales están en juego y así mismo deducir cuales hacen falta.
</br>
</br>
<div style="text-align:center"><img src="screenshots/salieron.png" /></div>
</br>
</br>
En la parte central de la ventana, podremos observar el método del juego que escogimos en la parte de configuraciones, si escogimos método automático, podremos manipular únicamente el botón rojo que indica: “Siguiente Numero”, este nos generara un numero aleatorio, que este aun disponible en la “tómbola”.
</br>
</br>
<div style="text-align:center"><img src="screenshots/siguiente.png" /></div>
</br>
</br>
Si escogimos método manual, podremos manipular el espacio para ingresar un numero personalizado, y pulsar el botón que india: “Ingresar número”.
</br>
</br>
<div style="text-align:center"><img src="screenshots/ingresar.png" /></div>
</br>
</br>
Los números que estén apareciendo, se colorearan de manera automática en todos los tableros, por lo cual no es necesario realizar ninguna acción, mucho menos preocuparse por si no se marco alguno que haya salido, porque es imposible. Si el numero se pinta de color amarillo quiere decir que este ya “salió”.
</br>
</br>
<div style="text-align:center"><img src="screenshots/tabla.png" /></div>
</br>
</br>
En la imagen anterior podemos observa un tablero en el cual contiene varios números coloreados con amarillo, estos números corresponden a dígitos que ya salieron.

La pantalla de juego está dividida en 2 grandes segmentos, “Jugador” y “Computer”, estas divisiones indican que el lado izquierdo pertenece al jugador y el lado derecho pertenece a los tableros correspondientes a la computadora.

En la parte superior de estas divisiones encontraremos la manera de desplazarnos entre tableros, ya sean entre los del jugador o los de la computadora.
</br>
</br>
<div style="text-align:center"><img src="screenshots/partida.png" /></div>
</br>
</br>
En la pantalla de juego también podremos encontrar 3 tabs, el primero llamado “Juego”, podremos visualizar la partida actual, el segundo es un conversor, por si deseamos realizar alguna conversión entre los sistemas numéricos soportados y el tercer y último tab nos permitirá ver la configuración actual de la partida.
</br>
</br>
Conversor:
</br>
</br>
<div style="text-align:center"><img src="screenshots/conversor.png" /></div>
</br>
</br>
Información de la partida:
</br>
</br>
<div style="text-align:center"><img src="screenshots/info.png" /></div>
</br>
</br>

## Ganadores
Al finalizar la partida, basado en el tipo de juego que hayamos escogido, vamos a observar la siguiente pantalla
</br>
</br>
<div style="text-align:center"><img src="screenshots/ganador.png" /></div>
</br>
</br>
La misma cuenta con 2 tabs, en el primero llamado “Ganador(es)” podremos navegar entre los tableros que hayan completado con la meta establecida, y en el segundo tab podremos observar el resumen de la partida que se estaba jugando.
</br>
</br>
<div style="text-align:center"><img src="screenshots/resumen.png" /></div>
</br>
</br>

<a id="raw-url" href="screenshots/Manual-de-usuario-Cristhian-Ruiz-Hernandez-BITCON.pdf">Descargar este Manual de usuario</a>